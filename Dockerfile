FROM wordpress

ENV LANG=C.UTF-8
ENV LC_ALL=C.UTF-8

RUN apt-get update
RUN apt-get install tmux vim bash rsync -y
RUN curl -sL https://deb.nodesource.com/setup_12.x | bash -
RUN apt-get install nodejs -y
RUN curl -o- -L https://yarnpkg.com/install.sh | bash

COPY mytheme /usr/src/wordpress/wp-content/themes/mytheme 
COPY myplugin /usr/src/wordpress/wp-content/plugins/myplugin
RUN chmod a+xrw -R /usr/src/wordpress/wp-content
