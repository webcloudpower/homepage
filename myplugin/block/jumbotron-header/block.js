(function (blocks, editor, components, i18n, element) {
  var el = wp.element.createElement
  var registerBlockType = wp.blocks.registerBlockType
  var RichText = wp.editor.RichText

  registerBlockType('myplugin/jumbotron-header', { // The name of our block. Must be a string with prefix. Example: my-plugin/my-custom-block.
    title: 'Jumbotron Header', // The title of our block.
    description: 'A jumbotron header',
    icon: 'archive',
    category: 'my-plugin',
    supports: {
      align: true,
      alignWide: true
    },
    attributes: {
      title: {
        type: 'array',
        source: 'children',
        selector: 'h1'
      },
      description: {
        type: 'array',
        source: 'children',
        selector: 'h2'
      }
    },

    edit: function (props) {
      var attributes = props.attributes

      return [
        el('div', { className: `${props.className} jumbotron` },
          el('div', { className: 'container' },
            el(RichText, {
              key: 'editable',
              tagName: 'h1',
              className: 'display-3',
              placeholder: 'Profile Name',
              keepPlaceholderOnFocus: true,
              value: attributes.title,
              onChange: function (newTitle) {
                props.setAttributes({ title: newTitle })
              }
            }),
            el(RichText, {
              key: 'editable',
              tagName: 'h2',
              placeholder: 'Description',
              keepPlaceholderOnFocus: true,
              value: attributes.description,
              onChange: function (newDescription) {
                props.setAttributes({ description: newDescription })
              }
            })
          )
        )
      ]
    },

    save: function (props) {
      var attributes = props.attributes

      return (
        el('div', { className: `${props.className} jumbotron` },
          el('div', { className: 'container' },
            el(RichText.Content, {
              tagName: 'h1',
              className: 'display-3',
              value: attributes.title
            }),
            el(RichText.Content, {
              tagName: 'h2',
              value: attributes.description
            })
          )
        )
      )
    }
  })

})(
  window.wp.blocks,
  window.wp.editor,
  window.wp.components,
  window.wp.i18n,
  window.wp.element
)
