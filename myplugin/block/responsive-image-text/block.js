(function (blocks, editor, components, i18n, element) {
  var el = wp.element.createElement
  var registerBlockType = wp.blocks.registerBlockType
  var RichText = wp.editor.RichText
	var MediaUpload = wp.editor.MediaUpload
  var InnerBlocks =  wp.editor.InnerBlocks

  registerBlockType('myplugin/myplugin-responsive-image-text', {
    title: 'Responsive image text',
    description: 'Responsive image text',
    icon: 'align-left',
    category: 'my-plugin',
    supports: {
      align: true,
      alignWide: true
    },
    attributes: {
      title: {
        type: 'array',
        source: 'children',
        selector: 'h2'
      },
      mediaID: {
        type: 'number'
      },
      mediaURL: {
        type: 'string',
        source: 'attribute',
        selector: 'img',
        attribute: 'src'
      },
    },

    edit: function (props) {
      var attributes = props.attributes

      var onSelectImage = function (media) {
        return props.setAttributes({
          mediaURL: media.url,
          mediaID: media.id
        })
      }

      return [
        el('div', { className: `${props.className}` },
          el('div', { className: 'row' },
            el('div', { className: 'col-md-6 order-md-2' },
              el(RichText, {
                key: 'editable',
                tagName: 'h1',
                placeholder: 'Title',
                keepPlaceholderOnFocus: true,
                value: attributes.title,
                onChange: function (newTitle) {
                  props.setAttributes({ title: newTitle })
                }
              }),
              el(InnerBlocks)
						),
            el('div', { className: 'col-md-6 order-md-1' },
              el(MediaUpload, {
                onSelect: onSelectImage,
                type: 'image',
                value: attributes.mediaID,
                render: function (obj) {
                  return el(components.Button, {
                    className: attributes.mediaID ? 'image-button' : 'button button-large',
                    onClick: obj.open
                  },
                  !attributes.mediaID ? 'Upload Image' : el('img', { src: attributes.mediaURL })
                  )
                }
              })
					  )
					)
        )
      ]
    },

    save: function (props) {
      var attributes = props.attributes

      return (
        el('div', { className: `${props.className}` },
          el('div', { className: 'row' },
            el('div', { className: 'col-md-6 order-md-2' },
              el(RichText.Content, {
                tagName: 'h2',
                value: attributes.title
              }),
              el(InnerBlocks.Content)
						),
            el('div', { className: 'col-md-6 order-md-1' },
						  el('img', {
							  src: attributes.mediaURL,
								className: 'img-thumbnail',
							})
					  )
					)
        )
      )
    }
  })

})(
  window.wp.blocks,
  window.wp.editor,
  window.wp.components,
  window.wp.i18n,
  window.wp.element
)
