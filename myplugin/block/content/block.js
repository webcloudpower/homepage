(function (blocks, editor, components, i18n, element) {
  var el = wp.element.createElement
  var registerBlockType = wp.blocks.registerBlockType
  var RichText = wp.editor.RichText
  var InnerBlocks =  wp.editor.InnerBlocks

  registerBlockType('myplugin/content', {
    title: 'Myplugin content',
    description: 'My plugin content',
    icon: 'editor-quote',
    category: 'my-plugin',
    supports: {
      align: true,
      alignWide: true
    },
    edit: function (props) {
      return [
        el('div', { className: `${props.className} container` },
          el(InnerBlocks)
        )
      ]
    },

    save: function (props) {
      return (
        el('div', { className: `${props.className} container` },
          el(InnerBlocks.Content)
        )
      )
    }
  })

})(
  window.wp.blocks,
  window.wp.editor,
  window.wp.components,
  window.wp.i18n,
  window.wp.element
)
