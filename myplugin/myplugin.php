<?php
/**
 *
 * @link https://organicthemes.com
 * @since 1.0.0
 * @package OPB
 *
 * Plugin Name: Myplugin
 * Plugin URI: https://organicthemes.com/
 * Description: The Profile Block is created for the Gutenberg content editor. It displays a profile section with an image, name, subtitle, bio and personal social media links. It's perfect for author biographies, personal profiles, or staff pages.
 * Author: Organic Themes
 * Author URI: https://organicthemes.com/
 * Version: 1.3.1
 * License: GPL2+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Define global constants.
 *
 * @since 1.0.0
 */
// Plugin version.
if ( ! defined( 'OPB_VERSION' ) ) {
	define( 'OPB_VERSION', '1.3.1' );
}

if ( ! defined( 'OPB_NAME' ) ) {
	define( 'OPB_NAME', trim( dirname( plugin_basename( __FILE__ ) ), '/' ) );
}

if ( ! defined( 'OPB_DIR' ) ) {
	define( 'OPB_DIR', WP_PLUGIN_DIR . '/' . OPB_NAME );
}

if ( ! defined( 'OPB_URL' ) ) {
	define( 'OPB_URL', WP_PLUGIN_URL . '/' . OPB_NAME );
}

function myplugin() {
	wp_enqueue_style(
		'myplugin-bootstrap', // Handle.
		plugins_url( 'bootstrap.css', __FILE__ ), // Font Awesome for social media icons.
		array(),
		'4.7.0'
	);
	wp_enqueue_style(
		'myplugin-font-awesone', // Handle.
		plugins_url( 'font-awesome.css', __FILE__ ), // Font Awesome for social media icons.
		array(),
		'4.7.0'
	);
	wp_enqueue_style(
		'myplugin-custom', // Handle.
		plugins_url( 'custom.css', __FILE__ ), // Font Awesome for social media icons.
		array(),
		'4.7.0'
	);
  add_filter( 'block_categories', function( $categories, $post ) {
    return array_merge(
      $categories,
      array(
        array(
          'slug'  => 'my-plugin',
          'title' => 'My plugin',
        ),
      )
    );
  }, 10, 2 );

  $args = array(
    'public' => true,
    'label' => 'Blog',
    'has_archive' => 'blog',
    'show_in_rest' => true,
    'supports' => array(
      'title',
      'editor',
      'excerpt',
      'author',
      'revisions'
    )
  );
  register_post_type( 'blog-post', $args );
};

add_action( 'init', 'myplugin' );
require_once( OPB_DIR . '/block/content/index.php' );
require_once( OPB_DIR . '/block/jumbotron-header/index.php' );
require_once( OPB_DIR . '/block/responsive-image-text/index.php' );
require_once( OPB_DIR . '/block/responsive-text-image/index.php' );
require_once( OPB_DIR . '/block/row/index.php' );
