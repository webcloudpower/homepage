    <footer class="footer">
      <div class="container">
        <div class="row">
          <div class="col-12 col-sm-5 col-md-4">
            <span class="text-muted">aronwolf90@gmail.com</span>
          </div>
          <div class="col-12 col-sm-5 col-md-4">
            <span class="text-muted">main developer: Aron Wolf</span>
          </div>
          <div class="col-12 col-sm-2 col-md-4">
            <a class="text-muted" href="/privacy">privacy policy</a>
          </div>
        </div>
      </div>
    </footer>
    
    <script src="<?php echo get_bloginfo('template_directory');?>/dist/main.js" rel="stylesheet"></script>
    <?php wp_footer(); ?>
  </body>
</html>
