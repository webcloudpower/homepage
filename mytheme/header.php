<html>
  <head>
    <title><?php wp_title(); echo $GLOBALS['header_sufix']?></title>
    <link href="<?php echo get_bloginfo('template_directory');?>/dist/main.css" rel="stylesheet">
    <?php wp_head();?>
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <a class="navbar-brand" href="/">Home</a>
      <button
        aria-controls="navbarSupportedContent"
        aria-expanded="false"
        aria-label="Toggle navigation"
        class="navbar-toggler"
        data-target="#navbarSupportedContent"
        data-toggle="collapse"
        type="button"
      >
        <span class="navbar-toggler-icon"></span>
      </button>
      <div
        class="collapse navbar-collapse" 
        id="navbarSupportedContent"
      >
        <?php
          wp_nav_menu( array(
          	'theme_location'  => 'primary',
          	'depth'	          => 2, // 1 = no dropdowns, 2 = with dropdowns.
          	'container'       => 'div',
          	'container_class' => 'mr-auto',
          	'container_id'    => 'bs-example-navbar-collapse-1',
          	'menu_class'      => 'navbar-nav',
          	'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
          	'walker'          => new WP_Bootstrap_Navwalker(),
          )); 
        ?>
      </div>
    </nav>
