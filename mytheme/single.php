<?php /* Template Name: CustomPageT1 */ ?>
 
<?php get_header(); ?>

<div class="jumbotron">
  <div class="container">
    <h1 class="display-3"><?php the_title();?></h1>
  </div>
</div>

<div class="container-fluid"> 
  <div class="non-header">
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
      <?php the_content();?>
    <?php endwhile; endif; ?>
  </div>
</div>
 
<?php get_footer(); ?>

