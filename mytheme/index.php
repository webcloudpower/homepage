<?php /* Template Name: CustomPageT1 */ ?>
 
<?php get_header(); ?>

<div class="jumbotron">
  <div class="container">
    <h1 class="display-3">WebCloudPower</h1>
    <p>Blog for programmers about web, cloud and kubernetics</p>
  </div>
</div>

<div class="container"> 
  <div class="non-header">
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
      <a class="text-dark" href="<?php the_permalink() ?>"><h2><?php the_title();?></h2></a>
      <?php the_excerpt();?>
    <?php endwhile; endif; ?>
  </div>
</div>
 
<?php get_footer(); ?>

